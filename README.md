# Prueba RECYLINK

¡Hola, un placer!

Primero que todo agradezco la oportunidad por presentar esta prueba, me gusta bastante los retos lógicos asi que estuve bastante feliz en realizarla. Debo decir que utilice meramente JavaScript (NodeJS + Express)

Para iniciar la aplicación es sencillo, solo deben aplicar los siguientes comandos:

Con este comando instalaran las dependencias de la aplicación (nodemon y express)

```
npm install
```

Y ya con las dependencias instaladas simplemente realizamos el comando para iniciar la aplicación.

```
npm start
```

¡Y deberia de funcionar, de verdad, muchas gracias por la oportunidad, espero que sea de su agrado!