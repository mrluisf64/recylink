exports.default = (req, res) => {
  const rokket = (arrayFirst, arrayTwo) => {
    let arrayFusionated = arrayFirst.concat(arrayTwo);
    return arrayFusionated.filter((item, index) => {
      return arrayFusionated.indexOf(item) === index;
    });
  };

  /** AQUI PUEDES CAMBIAR LOS VALORES PARA PROBAR */

  let result = rokket([1, 2, 5], [2, 1, 6]);
  let result2 = rokket([1, 2, 3], [4, 5, 6]);

  console.log(result);

  res.send(
    "Los resultados de la quinta prueba son: <br>" +
      "Primera función: " +
      result +
      "<br>" +
      "Segunda función: " +
      result2 +
      "<br>" +
      "Para más detalles verifique en el archivo ansnwer/fiveAnswer.js"
  );
};
