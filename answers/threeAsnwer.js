exports.default = (req, res) => {
  const rokket = (value, maxRepeat) => {
    let newValue = "";
    for (let i = 1; i <= maxRepeat; i++) {
      newValue += value;
    }
    return newValue;
  };

  /** AQUI PUEDE CAMBIAR VALORES PARA PROBAR */

  let result = rokket("node", 5);
  let result2 = rokket("abc", 2);

  console.log(result, result2);

  res.send(
    "Los resultados de la tercera prueba son: <br>" +
      "Primera función: " +
      result +
      "<br>" +
      "Segunda función: " +
      result2 +
      "<br>" +
      "Para más detalles verifique en el archivo ansnwer/threeAnswer.js"
  );
};
