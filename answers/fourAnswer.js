exports.default = (req, res) => {
  /** AQUI PUEDES AGREGAR O ELIMINAR OBJECTOS PARA PROBAR */
  const contacts = [
    { firstName: "Juanito", lastName: "Rokket" },
    { firstName: "James", lastName: "Bond" },
    { firstName: "Harry", lastName: "Potter" }
  ];

  const rokket = contacts => {
    return contacts.map(contact => {
      return contact.lastName;
    });
  };

  let result = rokket(contacts);

  console.log(result);

  res.send(
    "Lo resultados de la cuarta prueba son: " +
      result +
      "<br>" +
      "Para más detalles verifique en el archivo ansnwer/fourAnswer.js"
  );
};
