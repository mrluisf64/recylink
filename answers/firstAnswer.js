exports.default = (req, res) => {
  const rokket = x => {
    return y => {
      return z => {
        return x * y * z;
      };
    };
  };

  /** PUEDES CAMBIARLO ACÁ PARA PROBAR CON DIFERENTES OPCIONES */
  let result = rokket(2)(5)(3);
  let result2 = rokket(4)(2)(2);
  let result3 = rokket(8)(2)(1);
  console.log(result, result2, result3);

  res.send(
    "Los resultados de la primera prueba son: <br>" +
      "Primera función: " +
      result +
      "<br>" +
      "Segunda función: " +
      result2 +
      "<br>" +
      "Ultima función: " +
      result3 +
      "<br>" +
      "Para más detalles verifique en el archivo ansnwer/firstAnswer.js"
  );
};
