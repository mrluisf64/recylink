const express = require("express");
const app = express();
const port = 3000;

const firstAsnwer = require("./answers/firstAnswer");
const fiveAnswer = require("./answers/fiveAnswer");
const fourAnswer = require("./answers/fourAnswer");
const secondAnswer = require("./answers/secondAnswer");
const threeAnswer = require("./answers/threeAsnwer");

app.get("/", (req, res) => {
  res.send(
    "Hola, un placer!, las rutas para verificar las pruebas son: first, second, three, four y five. Un ejemplo: localhost:3000/first, ¡muchas gracias por la oportunidad!"
  );
});
app.get("/first", firstAsnwer.default);
app.get("/second", secondAnswer.default);
app.get("/three", threeAnswer.default);
app.get("/four", fourAnswer.default);
app.get("/five", fiveAnswer.default);

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
